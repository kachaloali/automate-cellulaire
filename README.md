Qu'est-ce qu'un automate cellulaire ?
====================
Un automate cellulaire consiste en une grille régulière de **cellules** contenant chacune un **état** choisi parmi un ensemble 
fini et qui peut évoluer au cours du temps. L'état d'une cellule au temps **t+1** est fonction de l'état au temps **t** d'un nombre 
fini de cellules appelé son **voisinage**.

Objectif du projet
===================
Ce projet consiste en la réalisation d'un automate cellulaire un peu plus élaboré que le 
<a href="https://fr.wikipedia.org/wiki/Jeu_de_la_vie"> jeu de la vie </a>, où les cellules se déplacent 
dans un monde à deux dimensions, et où s'organise un algorithme génétique qui favorise certains comportements. 
Pour en savoir plus sur ce projet, veuillez consulter le lien suivant:

# https://kachaloali.gitlab.io/automate-cellulaire/